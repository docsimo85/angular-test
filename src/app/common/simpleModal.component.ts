import {Component, ElementRef, Inject, Input, ViewChild} from "@angular/core";
import {JQ_TOKEN} from "./jQuery.service";

@Component({
  selector: 'simple-modal',
  templateUrl: './simpleModal.component.html',
  styleUrls: ['./simpleModal.component.css']
})

export class SimpleModalComponent {
  @Input() title: string | undefined;
  @Input() elementId: string | undefined
  @Input() closeOnBodyClick: string | undefined
  @ViewChild('modalContainer') containerEl: ElementRef | undefined;

  constructor(@Inject(JQ_TOKEN) private $:any) {
  }

  closeModal() {
    if (this.closeOnBodyClick?.toLocaleLowerCase()==='true') {
      this.$(this.containerEl?.nativeElement).modal('hide');
    }
  }
}
