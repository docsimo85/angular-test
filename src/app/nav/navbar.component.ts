import {Component, OnInit} from '@angular/core';
import {EventService, ISession} from "../events";
import {AuthService} from "../user/auth.service";

@Component({
  selector: 'nav-bar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  searchTerm: string = '';
  foundSession: ISession[] | any;

  constructor(private eventService: EventService, public auth: AuthService) {
  }


  searchSessions(searchTerm: string) {
    this.eventService.searchSessions(searchTerm).subscribe
    (sessions => {
      this.foundSession = sessions;
    })
  }

  ngOnInit(): void {
  }

}
