import {IUser} from "./user.model";
import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, tap} from "rxjs/operators";
import {of} from "rxjs";

@Injectable()
export class AuthService {
  currentUser: IUser | any

  constructor(private http: HttpClient) {
  }

  loginUser(userName: string, password: string) {

    let loginInfo = {username: userName, password: password};
    let options = {headers: new HttpHeaders({'Content-type': 'application/json'})}

    return this.http.post("/api/login", loginInfo, options)
      .pipe(tap(data => {
        // @ts-ignore
        this.currentUser = <IUser>data["user"]
      }))
      .pipe(catchError(err => {
        return of(false)
      }))
    // this.currentUser = {
    //   id: 89,
    //   nome: 'Simo',
    //   cognome: 'Gaido',
    //   userName: userName
    // }
  }

  isAuthenticated() {
    return !!this.currentUser;
  }

  checkAuthenticationStatus() {
    this.http.get("/api/currentIdentity")
      .pipe(tap(data => {
        if (data instanceof Object) {
          this.currentUser = <IUser>data;
        }
      }))
      .subscribe()
  }

  updateCurrentUser(nome: string, cognome: string) {

    this.currentUser.firstName = nome;
    this.currentUser.lastName = cognome;

    let options = {headers: new HttpHeaders({'Content-type': 'application/json'})};
    return this.http.put(`/api/users/${this.currentUser.id}`, this.currentUser, options)

  }

  logout(){
    this.currentUser = undefined;
    let options = {headers: new HttpHeaders({'Content-type': 'application/json'})};
    return this.http.post("/api/logout", {}, options);
  }
}
