import {Component, OnInit} from "@angular/core";
import {AuthService} from "./auth.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit{
  loginForm!: FormGroup;
  private username!:FormControl
  private password!: FormControl
  loginInvalid = false;

  constructor(private authService:AuthService,
              private route: Router) {
  }

  ngOnInit() {
    this.username = new FormControl('', [Validators.required]);
    this.password = new FormControl('', [Validators.required]);
    this.loginForm = new FormGroup({
      username: this.username,
      password: this.password
    })
  }

  login(formValues:any) {
    this.authService.loginUser(formValues.username, formValues.password)
      .subscribe(resp => {
        if (!resp){
          this.loginInvalid = true;
        }
        else {
          this.route.navigate(['/events'])
        }
      })
  }
}
