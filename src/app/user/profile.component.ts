import {Component, Inject, OnInit} from '@angular/core'
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {Toastr, TOASTR_TOKEN} from "../common";

@Component({
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {
  profileForm!: FormGroup;
  private nome!:FormControl
  private cognome!: FormControl

  constructor(private router: Router,
              private authService: AuthService,
              @Inject(TOASTR_TOKEN) private toastr:Toastr ) {
  }

  ngOnInit() {
    this.nome = new FormControl(this.authService.currentUser.firstName, [Validators.required]);
    this.cognome = new FormControl(this.authService.currentUser.lastName, [Validators.required]);
    this.profileForm = new FormGroup({
      nome: this.nome,
      cognome: this.cognome
    })
  }

  cancel() {
    this.router.navigate(['events'])
  }

  saveProfile(formValues: any) {
    this.authService.updateCurrentUser(formValues.nome, formValues.cognome)
      .subscribe(()=> {
        this.toastr.success('User data updated')
      })
  }

  logout() {
    this.authService.logout().subscribe(()=> {
      this.router.navigate(['/user/login'])
    })
  }

}
