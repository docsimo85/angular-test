import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {EventsAppComponent} from "./events-app.component";
import {NavbarComponent} from "./nav/navbar.component";
import {
  CollapsibleWellComponent,
  JQ_TOKEN,
  ModalTriggerDirective,
  SimpleModalComponent,
  Toastr,
  TOASTR_TOKEN
} from "./common";
import {RouterModule} from "@angular/router";
import {appRoutes} from "./router";
import {Error404Component} from "./errors/404.component";
import {HttpClientModule} from "@angular/common/http"
import {
  CreateEventComponent,
  CreateSessionComponent,
  DurationPipe,
  EventDetailsComponent,
  EventListComponent,
  EventListResolverService,
  EventResolverService,
  EventService,
  EventThumbnailComponent,
  SessionListComponent,
  UpvoteComponent,
  VoterService,
} from './events'
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthService} from "./user/auth.service";

// @ts-ignore
let toastr: Toastr = window['toastr'];
// @ts-ignore
let jQuery = window['$'];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    EventsAppComponent,
    EventListComponent,
    EventThumbnailComponent,
    NavbarComponent,
    EventDetailsComponent,
    CreateEventComponent,
    Error404Component,
    CreateSessionComponent,
    SessionListComponent,
    CollapsibleWellComponent,
    DurationPipe,
    SimpleModalComponent,
    ModalTriggerDirective,
    UpvoteComponent,
  ],
  // nota: i service vanno dichiarati nei providers
  providers: [
    EventService,
    {
      provide: TOASTR_TOKEN,
      useValue: toastr
    },
    EventResolverService,
    {
      provide: 'canDeactivateCreateEvent',
      useValue: checkDirtyState
    },
    {
      provide: JQ_TOKEN,
      useValue: jQuery
    },
    EventListResolverService,
    AuthService,
    VoterService,
  ],
  bootstrap: [EventsAppComponent]
})
export class AppModule {
}

export function checkDirtyState(component: CreateEventComponent) {
  if (component.isDirty) {
    return window.confirm('Non hai salvato i dati, sei sicuro di voler lasciare la pagina?')
  }
  return true;
}
