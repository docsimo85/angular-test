import {EventEmitter, Injectable} from "@angular/core";
import {Observable, of, Subject} from "rxjs";
import {IEvent, ISession} from "./event.model";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError} from "rxjs/operators";

@Injectable()
export class EventService {

  constructor(private http: HttpClient) {
  }

  getEvents(): Observable<IEvent[]> {
    return this.http.get<IEvent[]>('/api/events')
      .pipe(catchError(this.handleError<IEvent[]>('getEvents', [])))
  }

  getEvent(id: number): Observable<IEvent> | any {
    return this.http.get<IEvent>('/api/events/' + id)
      .pipe(catchError(this.handleError<IEvent>('getEvent')))
  }

  saveEvent(event: any) {
    let options = {
      headers: new HttpHeaders({'Content-type': 'application/json'})
    }
    return this.http.post<any>("/api/events", event, options)
      .pipe(catchError(this.handleError<IEvent>('saveEvent')))
  }

  searchSessions(searchTerm: string): Observable<ISession[]> {
    return this.http.get<ISession[]>('/api/sessions/search?search=' + searchTerm)
      .pipe(catchError(this.handleError<ISession[]>('searchSessions')))
  }

  //handle error boilerplate/template
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      return of(result as T)
    }
  }
}
