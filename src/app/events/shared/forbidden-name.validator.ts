import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const forbiddden = nameRe.test(control.value);
    return forbiddden ? {forbiddenName: {value: control.value}}
      : null;
  };
}
