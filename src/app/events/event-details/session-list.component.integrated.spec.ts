import {ComponentFixture, TestBed} from "@angular/core/testing";
import {SessionListComponent} from "./session-list.component";
import {DebugElement, NO_ERRORS_SCHEMA} from "@angular/core";
import {AuthService} from "../../user/auth.service";
import {VoterService} from "./voter.service";
import {DurationPipe} from "../shared/duration.pipe";
import {By} from "@angular/platform-browser";

describe('sessionListComponent', ()=> {

  let mockAuthService:any,
    mockVoterService:any,
    fixture: ComponentFixture<SessionListComponent>,
    component: SessionListComponent,
    element: HTMLElement,
    debugEl: DebugElement

  beforeEach(() => {
    mockVoterService = { userHasVoted: () => true };
    mockAuthService = { isAuthenticated: () => true, currentUser: {
      userName: 'pippo'
      } };
    TestBed.configureTestingModule({
      declarations: [
        SessionListComponent,
        DurationPipe
      ],
      providers: [
        { provide: AuthService, useValue: mockAuthService},
        { provide: VoterService, useValue: mockVoterService}
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    fixture = TestBed.createComponent(SessionListComponent);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    element = fixture.nativeElement;
  })
  describe('initial display', () => {
    it('should have correct name',  () => {
      component.sessions = [
        {name: 'Session 1', id: 3, duration: 12, level: 'beginner', abstract: 'ciao',
        voters: ['john', 'bob']}
      ];
      component.filterBy = 'all';
      component.sortBy = 'name';
      component.eventId = 5;
      component.ngOnChanges();

      fixture.detectChanges();

      //test using element
      expect(element.querySelector('[well-title]')?.textContent).toContain('Session 1')

      //or using debugElement (Angular-aware)
      expect(debugEl.query(By.css('[well-title]')).nativeElement.textContent).toContain('Session 1')

    });
  })
})
