import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ISession} from "../shared";
import {forbiddenNameValidator} from "../shared/forbidden-name.validator";

@Component({
  selector: 'create-session',
  templateUrl: './create-session.component.html',
  styleUrls: ['./create-session.component.css']
})
export class CreateSessionComponent implements OnInit {

  newSessionForm = this.fb.group({
    name: ['', [Validators.required, forbiddenNameValidator(new RegExp('^bob$', 'i'))]],
    presenter: ['', Validators.required],
    duration: ['', Validators.required],
    level: ['', Validators.required],
    abstract: ['', Validators.required]
  })

  constructor(private fb: FormBuilder) {
  }
  @Output() saveNewSession = new EventEmitter;
  @Output() cancelAddSession = new EventEmitter;

  ngOnInit() {
  }

  saveSession(formValues: any) {
    let session: ISession = {
      name: formValues.name,
      duration: +formValues.duration,
      level: formValues.level,
      presenter: formValues.presenter,
      id: undefined,
      voters: [],
      abstract: formValues.abstract
    }
    this.saveNewSession.emit(session)
  }

  cancel() {
    this.cancelAddSession.emit()
  }

}
