import {VoterService} from "./voter.service";
import {ISession} from "../shared";
import {of} from "rxjs";

describe('voterservice', () => {
  let voterService: VoterService,
    mockHttp: any;

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj('mockHttp', ['delete', 'post'])
    voterService = new VoterService(mockHttp);
  })

  describe('deleteVoter', () => {

    it('should remove thew voter from the list', function () {
      var session = {id: 3, voters: ["joe", "pippo"]};
      mockHttp.delete.and.returnValue(of(false))

      voterService.deleteVoter(6, <ISession>session, "joe");

      expect(session.voters.length).toBe(1);
      expect(session.voters[0]).toBe("pippo");
    });
    it('should call http.delete with the right url', function () {
      var session = {id: 3, voters: ["joe", "pippo"]};
      mockHttp.delete.and.returnValue(of(false))

      voterService.deleteVoter(6, <ISession>session, "joe");

      expect(mockHttp.delete).toHaveBeenCalledWith('/api/events/6/sessions/3/voters/joe')
    });
  })

  describe('addVoter', () => {
    it('should call http.post with the right url', function () {
      var session = {id: 3, voters: ["pippo"]};
      mockHttp.post.and.returnValue(of(false))

      voterService.addVoter(6, <ISession>session, "joe");

      expect(mockHttp.post).toHaveBeenCalledWith('/api/events/6/sessions/3/voters/joe', {},
        jasmine.any(Object))
    })

  })
})
