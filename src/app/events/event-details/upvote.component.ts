import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'upvote',
  templateUrl:'./upvote.component.html',
  styleUrls:['./upvote.component.css']
})
export class UpvoteComponent implements OnInit {

  @Input() count: number | undefined
  @Input() set voted(val: any) {
    this.iconColor = val ? 'red' : 'white';
  }
  @Output() vote = new EventEmitter();
  iconColor: string | undefined;

  constructor() {
  }

  onClick() {
    this.vote.emit({})
  }

  ngOnInit(): void {
  }

}
