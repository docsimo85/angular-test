import {Component, OnInit} from "@angular/core";
import {EventService} from "./shared";
import {ActivatedRoute} from "@angular/router";

@Component({
  templateUrl: './event-list.component.html',
})

export class EventListComponent implements OnInit {
  events: any

  constructor(private eventService: EventService, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.events = this.route.snapshot.data['events']
  }

}

