import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, Validators} from "@angular/forms";
import {EventService} from "./shared";

@Component({
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.css']
})
export class CreateEventComponent implements OnInit {
  newEventForm = this.fb.group({
    name: ['', Validators.required],
    date: ['', Validators.required],
    time: ['', Validators.required],
    price: ['', Validators.required],
    location: this.fb.group({
      address: ['', Validators.required],
      city: ['', Validators.required],
      country: ['', Validators.required]
    }),
    onlineUrl: [''],
    imageUrl: ['', Validators.required]
  })
  isDirty: boolean = true;


  constructor(private router: Router, private readonly fb: FormBuilder, private eventService: EventService) {
  }

  ngOnInit(): void {
  }

  saveEvent(formValues: any) {
    this.eventService.saveEvent(formValues).subscribe(() => {
      this.isDirty = false
      this.router.navigate(['/events'])
    });
  }

  cancel() {
    this.router.navigate(['/events'])
  }

}
