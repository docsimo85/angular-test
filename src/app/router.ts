import {
  CreateEventComponent,
  CreateSessionComponent,
  EventDetailsComponent,
  EventListResolverService,
  EventResolverService,
  EventListComponent
} from "./events";
import {Routes} from "@angular/router";
import {Error404Component} from "./errors/404.component";

export const appRoutes:Routes = [
  { path: 'event/session/new', component: CreateSessionComponent},
  { path: 'events', component: EventListComponent,
    resolve: {events:EventListResolverService} },
  { path: 'events/:id', component: EventDetailsComponent ,
    resolve: {event: EventResolverService} },
  { path: 'event/new', component: CreateEventComponent,
    canDeactivate:['canDeactivateCreateEvent']},
  { path: '404', component: Error404Component },
  { path: '', redirectTo: '/events', pathMatch: 'full' },
  { path: 'user',
    loadChildren: () => import('./user/user.module')
      .then(m => m.UserModule)
  }
]
